const { Client } = require('pg');

async function getConnection() {
  const client = new Client({
    host: 'localhost',
    post: 5432,
    user: '',
    password: '',
    database: 'mystore',
  });
  await client.connect();
  return client;
}

module.exports = getConnection;
